const { Router } = require('express');
const getHeroes = require('../controllers/hero.controller');

const router = Router();

router.get('/', getHeroes);

module.exports = router;