const { request, response } = require('express');
const heroes = require('../data/heroes');

const getHeroes = async (req = request, res = response) => {
    try {
        res.json({
            status: 200,
            msg: 'OK',
            payload: await heroes
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            status: 500,
            msg: 'Internal server error',
            payload: []
        });
    }
}

module.exports = getHeroes